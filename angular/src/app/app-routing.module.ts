import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
  },

  {
    path: 'account',
    loadChildren: () => import('@abp/ng.account').then(m => m.AccountModule.forLazy()),
  },
  {
    path: 'identity',
    loadChildren: () => import('@abp/ng.identity').then(m => m.IdentityModule.forLazy()),
  },
  {
    path: 'tenant-management',
    loadChildren: () =>
      import('@abp/ng.tenant-management').then(m => m.TenantManagementModule.forLazy()),
  },
  {
    path: 'setting-management',
    loadChildren: () =>
      import('@abp/ng.setting-management').then(m => m.SettingManagementModule.forLazy()),
  },
  {
    path: 'categories',
    loadChildren: () => import('./category/category.module').then(m => m.CategoryModule),
  },
  {
    path: 'authors',
    loadChildren: () => import('./author/author.module').then(m => m.AuthorModule),
  },
  { path: 'shelves', loadChildren: () => import('./shelf/shelf.module').then(m => m.ShelfModule) },
  { path: 'books', loadChildren: () => import('./book/book.module').then(m => m.BookModule) },
  {
    path: 'rentals',
    loadChildren: () => import('./rental/rental.module').then(m => m.RentalModule),
  },
  { path: 'myRentals', loadChildren: () => import('./my-rental/my-rental.module').then(m => m.MyRentalModule) },
  { path: 'BookList', loadChildren: () => import('./user-interface-book-list/user-interface-book-list.module').then(m => m.UserInterfaceBookListModule) },
  // { path: 'categoryMostBooks', loadChildren: () => import('./category-most-book/category-most-book.module').then(m => m.CategoryMostBookModule) },
  // { path: 'mostRentUsers', loadChildren: () => import('./most-rent-user/most-rent-user.module').then(m => m.MostRentUserModule) },
  // { path: 'mostRentCategories', loadChildren: () => import('./most-rent-category/most-rent-category.module').then(m => m.MostRentCategoryModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule],
})
export class AppRoutingModule {}
