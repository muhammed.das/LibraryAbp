import { ListService, PagedResultDto, PermissionService } from '@abp/ng.core';
import { Confirmation, ConfirmationService, ToasterService } from '@abp/ng.theme.shared';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthorService } from '@proxy/app-service';
import { AuthorDto } from '@proxy/dtos/author';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.scss'],
  providers: [ListService],
})
export class AuthorComponent {
  author = { items: [], totalCount: 0 } as PagedResultDto<AuthorDto>;

  selectedAuthor = {} as AuthorDto;

  isModalOpen = false;

  formGroup: FormGroup;

  constructor(
    public readonly list: ListService,
    private authorService: AuthorService,
    private formBuilder: FormBuilder,
    private confirmation: ConfirmationService,
    private permissionService: PermissionService,
    private toastrService: ToasterService
  ) {}
  ngOnInit() {
    this.getListAuthor();
  }

  hasEditPermission: boolean = this.permissionService.getGrantedPolicy('ExampAbp.Library.Edit');
  hasDeletePermission: boolean = this.permissionService.getGrantedPolicy('ExampAbp.Library.Delete');

  getListAuthor() {
    const authorStreamCreator = query => this.authorService.getList(query);

    this.list.hookToQuery(authorStreamCreator).subscribe(response => {
      this.author = response;
    });
  }

  createAuthor() {
    this.selectedAuthor = {} as AuthorDto;
    this.buildForm();
    this.isModalOpen = true;
  }

  editAuthor(id: string) {
    this.authorService.get(id).subscribe(author => {
      this.selectedAuthor = author;
      this.buildForm();
      this.isModalOpen = true;
    });
  }

  buildForm() {
    this.formGroup = this.formBuilder.group({
      name: [this.selectedAuthor.name || '', Validators.required],
    });
  }
  save() {
    if (this.formGroup.invalid) {
      return;
    }

    if (this.selectedAuthor.id) {
      this.authorService.update(this.selectedAuthor.id, this.formGroup.value).subscribe(() => {
        this.isModalOpen = false;
        this.formGroup.reset();
        this.list.get();
        this.toastrService.success('Yazar başarıyla Güncellendi.');
      });
    } else {
      this.authorService.create(this.formGroup.value).subscribe(() => {
        this.isModalOpen = false;
        this.formGroup.reset();
        this.list.get();
        this.toastrService.success('Yazar başarıyla Eklendi.');
      });
    }
  }

  delete(id: string) {
    this.confirmation.warn('::AreYouSureToDelete', '::AreYouSure').subscribe(status => {
      if (status === Confirmation.Status.confirm) {
        this.authorService.delete(id).subscribe(() => this.list.get());
        this.toastrService.success('Yazar başarıyla Silindi.');
      }
    });
  }
}
