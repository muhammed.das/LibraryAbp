import {
  ListService,
  PagedAndSortedResultRequestDto,
  PagedResultDto,
  PermissionService,
} from '@abp/ng.core';
import { Confirmation, ConfirmationService, ToasterService } from '@abp/ng.theme.shared';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthorService, BookService, CategoryService } from '@proxy/app-service';
import { BookDto, CreateUpdateBookDto } from '@proxy/dtos/book';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss'],
  providers: [ListService],
})
export class BookComponent {
  book = { items: [], totalCount: 0 } as PagedResultDto<BookDto>;

  authorList: string[] = [];
  categoryList: string[] = [];

  searchTerm: string = '';

  selectedBook = {} as BookDto;

  isModalOpen = false;

  formGroup: FormGroup;

  constructor(
    public readonly list: ListService,
    private bookService: BookService,
    private formBuilder: FormBuilder,
    private confirmation: ConfirmationService,
    private authorService: AuthorService,
    private categoryService: CategoryService,
    private permissionService: PermissionService,
    private toastrService: ToasterService
  ) {}
  ngOnInit() {
    this.getListBook();
    this.getAuthorList();
    this.getCategoryList();
  }

  hasEditPermission: boolean = this.permissionService.getGrantedPolicy('ExampAbp.Library.Edit');
  hasDeletePermission: boolean = this.permissionService.getGrantedPolicy('ExampAbp.Library.Delete');

  searchBooks() {
    const input: PagedAndSortedResultRequestDto = {
      maxResultCount: 50,
    };

    if (this.searchTerm) {
      this.bookService
        .getFilterByNameByBookNameAndInput(this.searchTerm, input)
        .subscribe(response => {
          this.book = response;
        });
    } else {
      this.getListBook();
    }
  }
  onSearchTermChange() {
    if (!this.searchTerm) {
      this.getListBook();
    }
  }

  getAuthorList() {
    const input: PagedAndSortedResultRequestDto = {
      maxResultCount: 25,
    };

    this.authorService.getList(input).subscribe(response => {
      this.authorList = response.items.map(author => author.name);
    });
  }

  getCategoryList() {
    const input: PagedAndSortedResultRequestDto = {
      maxResultCount: 25,
    };

    this.categoryService.getList(input).subscribe(response => {
      this.categoryList = response.items.map(category => category.name);
    });
  }

  getListBook() {
    const bookStreamCreator = query => this.bookService.getList(query);

    this.list.hookToQuery(bookStreamCreator).subscribe(response => {
      this.book = response;
    });
  }

  createBook() {
    this.selectedBook = {} as BookDto;
    this.buildForm();
    this.isModalOpen = true;
  }

  editBook(id: string) {
    this.bookService.get(id).subscribe(book => {
      this.selectedBook = book;
      this.buildForm();
      this.isModalOpen = true;
    });
  }

  buildForm() {
    this.formGroup = this.formBuilder.group({
      name: [this.selectedBook.name || '', Validators.required],
      stock: [this.selectedBook.stock || '', Validators.required],
      numberPage: [this.selectedBook.numberPage || '', Validators.required],
      authorName: [this.selectedBook.authorName || '', Validators.required],
      shelFloor: [this.selectedBook.shelFloor || '', Validators.required],
      shelfNumber: [this.selectedBook.shelfNumber || '', Validators.required],
      categoryName: [this.selectedBook.categoryName || [], Validators.required],
    });
  }
  save() {
    if (this.formGroup.invalid) {
      return;
    }

    const createUpdateDto: CreateUpdateBookDto = {
      name: this.formGroup.value.name,
      stock: this.formGroup.value.stock,
      numberPage: this.formGroup.value.numberPage,
      authorName: this.formGroup.value.authorName,
      shelFloor: this.formGroup.value.shelFloor,
      shelfNumber: this.formGroup.value.shelfNumber,
      categoryName: this.formGroup.value.categoryName.map((category: string) => category.trim()),
    };

    if (this.selectedBook.id) {
      this.bookService.update(this.selectedBook.id, createUpdateDto).subscribe(() => {
        this.isModalOpen = false;
        this.formGroup.reset();
        this.list.get();
        this.toastrService.success('Kitap başarıyla Güncellendi.');
      });
    } else {
      this.bookService.create(createUpdateDto).subscribe(() => {
        this.isModalOpen = false;
        this.formGroup.reset();
        this.list.get();
        this.toastrService.success('Kitap başarıyla Eklendi.');
      });
    }
  }

  delete(id: string) {
    this.confirmation.warn('::AreYouSureToDelete', '::AreYouSure').subscribe(status => {
      if (status === Confirmation.Status.confirm) {
        this.bookService.delete(id).subscribe(() => this.list.get());
        this.toastrService.success('Kitap başarıyla Silindi.');
      }
    });
  }
}
