import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryMostBookComponent } from './category-most-book.component';

const routes: Routes = [{ path: '', component: CategoryMostBookComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryMostBookRoutingModule { }
