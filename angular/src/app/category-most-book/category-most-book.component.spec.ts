import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryMostBookComponent } from './category-most-book.component';

describe('CategoryMostBookComponent', () => {
  let component: CategoryMostBookComponent;
  let fixture: ComponentFixture<CategoryMostBookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryMostBookComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CategoryMostBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
