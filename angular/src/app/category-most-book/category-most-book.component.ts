import { Component } from '@angular/core';
import { BookCategoryService, BookService, CategoryService } from '@proxy/app-service';
import { CategoryDto } from '@proxy/dtos';
import { BookDto } from '@proxy/dtos/book';
import { BookCategoryDto } from '@proxy/dtos/book-category';

@Component({
  selector: 'app-category-most-book',
  templateUrl: './category-most-book.component.html',
  styleUrls: ['./category-most-book.component.scss'],
})
export class CategoryMostBookComponent {
  topCategories: { categoryName: string; bookCount: number }[] = [];
  chartData: any;

  constructor(
    private categoryService: CategoryService,
    private bookCategoryService: BookCategoryService,
    private bookService: BookService
  ) {}

  ngOnInit() {
    this.getTopCategoriesWithBookCount();
  }

  getTopCategoriesWithBookCount() {
    this.categoryService
      .getList({ sorting: null, skipCount: 0, maxResultCount: 1000 })
      .subscribe((categoriesResponse: any) => {
        const categories = categoriesResponse.items;
        const categoryIds = categories.map((category: CategoryDto) => category.id);
        const bookCounts: { [categoryId: string]: number } = {};

        this.bookService
          .getList({ sorting: null, skipCount: 0, maxResultCount: 1000 })
          .subscribe((booksResponse: any) => {
            const books = booksResponse.items;

            books.forEach((book: BookDto) => {
              book.categoryName.forEach((categoryName: string) => {
                const categoryId = categories.find((c: CategoryDto) => c.name === categoryName)?.id;
                if (categoryId && categoryIds.includes(categoryId)) {
                  if (bookCounts[categoryId]) {
                    bookCounts[categoryId] += 1;
                  } else {
                    bookCounts[categoryId] = 1;
                  }
                }
              });
            });

            this.topCategories = Object.keys(bookCounts).map(categoryId => {
              const categoryName =
                categories.find((c: CategoryDto) => c.id === categoryId)?.name || '';
              const bookCount = bookCounts[categoryId];
              return { categoryName, bookCount };
            });

            this.topCategories.sort((a, b) => b.bookCount - a.bookCount);
            this.topCategories = this.topCategories.slice(0, 5);
            this.chartData = {
              labels: this.topCategories.map(category => category.categoryName),
              datasets: [
                {
                  label: 'Kiralama Sayısı',
                  data: this.topCategories.map(category => category.bookCount),
                  backgroundColor: [
                    'rgb(15, 119, 64)',
                    'rgb(15, 149, 74)',
                    'rgb(15, 169, 64)',
                    'rgb(15, 189, 64)',
                    'rgb(15, 209, 64)',
                  ],
                },
              ],
            };
          });
      });
  }
}
