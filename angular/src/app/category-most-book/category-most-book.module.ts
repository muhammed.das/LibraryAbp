import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryMostBookRoutingModule } from './category-most-book-routing.module';
import { CategoryMostBookComponent } from './category-most-book.component';
import { SharedModule } from '../shared/shared.module';
import { ChartModule } from '@abp/ng.components/chart.js';

@NgModule({
  declarations: [CategoryMostBookComponent],
  imports: [SharedModule, CategoryMostBookRoutingModule, ChartModule],
  exports: [CategoryMostBookComponent],
})
export class CategoryMostBookModule {}
