import { ListService, PagedResultDto, PermissionService } from '@abp/ng.core';
import { Confirmation, ConfirmationService, ToasterService } from '@abp/ng.theme.shared';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '@proxy/app-service';
import { CategoryDto } from '@proxy/dtos';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
  providers: [ListService],
})
export class CategoryComponent {
  category = { items: [], totalCount: 0 } as PagedResultDto<CategoryDto>;

  selectedCategory = {} as CategoryDto;

  isModalOpen = false;

  formGroup: FormGroup;

  constructor(
    public readonly list: ListService,
    private categoryService: CategoryService,
    private formBuilder: FormBuilder,
    private confirmation: ConfirmationService,
    private permissionService: PermissionService,
    private toastrService: ToasterService
  ) {}
  ngOnInit() {
    this.getListCategory();
  }

  hasEditPermission: boolean = this.permissionService.getGrantedPolicy('ExampAbp.Library.Edit');
  hasDeletePermission: boolean = this.permissionService.getGrantedPolicy('ExampAbp.Library.Delete');

  getListCategory() {
    const categoryStreamCreator = query => this.categoryService.getList(query);

    this.list.hookToQuery(categoryStreamCreator).subscribe(response => {
      this.category = response;
    });
  }

  createCategory() {
    this.selectedCategory = {} as CategoryDto;
    this.buildForm();
    this.isModalOpen = true;
  }

  editCategory(id: string) {
    this.categoryService.get(id).subscribe(category => {
      this.selectedCategory = category;
      this.buildForm();
      this.isModalOpen = true;
    });
  }

  buildForm() {
    this.formGroup = this.formBuilder.group({
      name: [this.selectedCategory.name || '', Validators.required],
    });
  }
  save() {
    if (this.formGroup.invalid) {
      return;
    }

    const request = this.selectedCategory.id
      ? this.categoryService.update(this.selectedCategory.id, this.formGroup.value)
      : this.categoryService.create(this.formGroup.value);

    request.subscribe(() => {
      this.isModalOpen = false;
      this.formGroup.reset();
      this.list.get();
      this.toastrService.success('İşlem başarıyla Gerçekleşti.');
    });
  }

  delete(id: string) {
    this.confirmation.warn('::AreYouSureToDelete', '::AreYouSure').subscribe(status => {
      if (status === Confirmation.Status.confirm) {
        this.categoryService.delete(id).subscribe(() => this.list.get());
        this.toastrService.success('Kategori Başarıyla Silindi');
      }
    });
  }
}
