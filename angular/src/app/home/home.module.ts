import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { MostRentBookComponent } from '../most-rent-book/most-rent-book.component';
import { MostRentCategoryModule } from '../most-rent-category/most-rent-category.module';
import { MostRentUserModule } from '../most-rent-user/most-rent-user.module';
import { CategoryMostBookModule } from '../category-most-book/category-most-book.module';

@NgModule({
  declarations: [HomeComponent, MostRentBookComponent],
  imports: [
    SharedModule,
    HomeRoutingModule,
    MostRentCategoryModule,
    MostRentUserModule,
    CategoryMostBookModule,
  ],
})
export class HomeModule {}
