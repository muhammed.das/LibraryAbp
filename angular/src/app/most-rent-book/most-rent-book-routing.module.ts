import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MostRentBookComponent } from './most-rent-book.component';

const routes: Routes = [{ path: '', component: MostRentBookComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MostRentBookRoutingModule { }
