import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostRentBookComponent } from './most-rent-book.component';

describe('MostRentBookComponent', () => {
  let component: MostRentBookComponent;
  let fixture: ComponentFixture<MostRentBookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostRentBookComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MostRentBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
