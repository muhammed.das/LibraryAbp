import { PagedResultDto } from '@abp/ng.core';
import { Component } from '@angular/core';
import { RentalService, RentalUserService } from '@proxy/app-service';
import { RentalDto } from '@proxy/dtos/rental';
import { RentalUserDto } from '@proxy/dtos/rental-user';

@Component({
  selector: 'app-most-rent-book',
  templateUrl: './most-rent-book.component.html',
  styleUrls: ['./most-rent-book.component.scss'],
})
export class MostRentBookComponent {
  mostRentedBooks: { bookName: string; rentalCount: number }[] = [];

  constructor(private rentalService: RentalService) {}

  ngOnInit(): void {
    this.loadMostRentedBooks();
  }

  loadMostRentedBooks(): void {
    this.rentalService
      .getList({ sorting: '', skipCount: 0, maxResultCount: 100 })
      .subscribe((response: PagedResultDto<RentalDto>) => {
        const rentals = response.items;

        const rentalCounts = {};
        for (let rental of rentals) {
          const bookName = rental.bookName;
          rentalCounts[bookName] = (rentalCounts[bookName] || 0) + 1;
        }

        this.mostRentedBooks = Object.keys(rentalCounts)
          .map(bookName => ({ bookName, rentalCount: rentalCounts[bookName] }))
          .sort((a, b) => b.rentalCount - a.rentalCount)
          .slice(0, 5);
      });
  }
}
