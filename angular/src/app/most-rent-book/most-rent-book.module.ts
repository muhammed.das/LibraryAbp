import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MostRentBookRoutingModule } from './most-rent-book-routing.module';
import { MostRentBookComponent } from './most-rent-book.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [],
  imports: [SharedModule, MostRentBookRoutingModule],
})
export class MostRentBookModule {}
