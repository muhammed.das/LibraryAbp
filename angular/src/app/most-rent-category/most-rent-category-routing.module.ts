import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MostRentCategoryComponent } from './most-rent-category.component';

const routes: Routes = [{ path: '', component: MostRentCategoryComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MostRentCategoryRoutingModule { }
