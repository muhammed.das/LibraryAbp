import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostRentCategoryComponent } from './most-rent-category.component';

describe('MostRentCategoryComponent', () => {
  let component: MostRentCategoryComponent;
  let fixture: ComponentFixture<MostRentCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostRentCategoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MostRentCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
