import { Component } from '@angular/core';
import { BookService, RentalService } from '@proxy/app-service';
import { BookDto } from '@proxy/dtos/book';
import { RentalDto } from '@proxy/dtos/rental';

@Component({
  selector: 'app-most-rent-category',
  templateUrl: './most-rent-category.component.html',
  styleUrls: ['./most-rent-category.component.scss'],
})
export class MostRentCategoryComponent {
  topCategories: { categoryName: string; rentalCount: number }[] = [];
  chartData: any;

  constructor(private rentalService: RentalService, private bookService: BookService) {}

  ngOnInit() {
    this.getTopCategoriesWithRentalCount();
  }

  getTopCategoriesWithRentalCount() {
    this.rentalService
      .getList({ sorting: null, skipCount: 0, maxResultCount: 1000 })
      .subscribe((rentalsResponse: any) => {
        const rentals = rentalsResponse.items;
        const categoryCounts: { [categoryName: string]: number } = {};

        rentals.forEach((rental: RentalDto) => {
          const bookName = rental.bookName;
          if (bookName) {
            this.bookService
              .getList({ sorting: null, skipCount: 0, maxResultCount: 1000 })
              .subscribe((booksResponse: any) => {
                const books = booksResponse.items;
                const book = books.find((b: BookDto) => b.name === bookName);
                if (book) {
                  const categories = book.categoryName;
                  categories.forEach((category: string) => {
                    if (categoryCounts[category]) {
                      categoryCounts[category] += 1;
                    } else {
                      categoryCounts[category] = 1;
                    }
                  });

                  this.topCategories = Object.keys(categoryCounts).map(categoryName => {
                    const rentalCount = categoryCounts[categoryName];
                    return { categoryName, rentalCount };
                  });

                  this.topCategories.sort((a, b) => b.rentalCount - a.rentalCount);
                  this.topCategories = this.topCategories.slice(0, 5);

                  this.chartData = {
                    labels: this.topCategories.map(category => category.categoryName),
                    datasets: [
                      {
                        label: 'Kiralama Sayısı',
                        data: this.topCategories.map(category => category.rentalCount),
                        backgroundColor: [
                          'rgb(20, 150, 64)',
                          'rgb(23, 170, 74)',
                          'rgb(25, 190, 64)',
                          'rgb(27, 210, 64)',
                          'rgb(32, 230, 64)',
                        ],
                      },
                    ],
                  };
                }
              });
          }
        });
      });
  }
}
