import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartModule } from '@abp/ng.components/chart.js';

import { MostRentCategoryRoutingModule } from './most-rent-category-routing.module';
import { MostRentCategoryComponent } from './most-rent-category.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [MostRentCategoryComponent],
  imports: [SharedModule, MostRentCategoryRoutingModule, ChartModule],
  exports: [MostRentCategoryComponent],
})
export class MostRentCategoryModule {}
