import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MostRentUserComponent } from './most-rent-user.component';

const routes: Routes = [{ path: '', component: MostRentUserComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MostRentUserRoutingModule {}
