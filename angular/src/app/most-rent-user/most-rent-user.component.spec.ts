import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostRentUserComponent } from './most-rent-user.component';

describe('MostRentUserComponent', () => {
  let component: MostRentUserComponent;
  let fixture: ComponentFixture<MostRentUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostRentUserComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MostRentUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
