import { Component } from '@angular/core';
import { RentalService } from '@proxy/app-service';
import { RentalDto } from '@proxy/dtos/rental';

@Component({
  selector: 'app-most-rent-user',
  templateUrl: './most-rent-user.component.html',
  styleUrls: ['./most-rent-user.component.scss'],
})
export class MostRentUserComponent {
  topUsers: { userName: string; rentedBooks: number }[] = [];

  chartData: any;

  chartOptions: any;

  constructor(private rentalService: RentalService) {}

  ngOnInit() {
    this.getTopUsersWithRentedBooks();
  }

  getTopUsersWithRentedBooks() {
    this.rentalService
      .getList({ sorting: null, skipCount: 0, maxResultCount: 1000 })
      .subscribe((rentalsResponse: any) => {
        const rentals = rentalsResponse.items;
        const userCounts: { [userName: string]: number } = {};

        rentals.forEach((rental: RentalDto) => {
          const userName = rental.userName;
          if (userName) {
            if (userCounts[userName]) {
              userCounts[userName] += 1;
            } else {
              userCounts[userName] = 1;
            }
          }
        });

        this.topUsers = Object.keys(userCounts).map(userName => {
          const rentedBooks = userCounts[userName];
          return { userName, rentedBooks };
        });

        this.topUsers.sort((a, b) => b.rentedBooks - a.rentedBooks);
        this.topUsers = this.topUsers.slice(0, 5);
        this.chartData = {
          labels: this.topUsers.map(user => user.userName),
          datasets: [
            {
              label: 'Kiralama Sayısı',
              data: this.topUsers.map(count => count.rentedBooks),
              backgroundColor: [
                'rgb(15, 119, 64)',
                'rgb(15, 149, 74)',
                'rgb(15, 169, 64)',
                'rgb(15, 189, 64)',
                'rgb(15, 209, 64)',
              ],
              borderColor: [
                'rgb(15, 119, 64)',
                'rgb(15, 149, 74)',
                'rgb(15, 169, 64)',
                'rgb(15, 189, 64)',
                ,
              ],
              borderWidth: 2, // Çizgi kalınlığı
            },
          ],
        };

        this.chartOptions = {
          scales: {
            x: {
              ticks: {
                maxRotation: 30, // Etiketlerin döngüsel kesilmesini önler
                minRotation: 30, // Etiketlerin döngüsel kesilmesini önler
              },
            },
          },

          elements: {
            point: {
              radius: 4, // Nokta büyüklüğü
              hoverRadius: 8, // Noktanın üzerine gelindiğinde büyüklüğü
            },
          },
        };
      });
  }
}
