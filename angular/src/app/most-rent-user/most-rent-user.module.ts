import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MostRentUserRoutingModule } from './most-rent-user-routing.module';
import { MostRentUserComponent } from './most-rent-user.component';
import { ChartModule } from '@abp/ng.components/chart.js';

@NgModule({
  declarations: [MostRentUserComponent],
  imports: [CommonModule, MostRentUserRoutingModule, ChartModule],
  exports: [MostRentUserComponent],
})
export class MostRentUserModule {}
