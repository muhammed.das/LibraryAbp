import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyRentalComponent } from './my-rental.component';

const routes: Routes = [{ path: '', component: MyRentalComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyRentalRoutingModule { }
