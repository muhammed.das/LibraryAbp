import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRentalComponent } from './my-rental.component';

describe('MyRentalComponent', () => {
  let component: MyRentalComponent;
  let fixture: ComponentFixture<MyRentalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyRentalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MyRentalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
