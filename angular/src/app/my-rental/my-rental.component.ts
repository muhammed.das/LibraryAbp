import { ConfigStateService, PagedAndSortedResultRequestDto } from '@abp/ng.core';
import { formatDate } from '@angular/common';
import { Component } from '@angular/core';
import { RentalService } from '@proxy/app-service';
import { RentalDto } from '@proxy/dtos/rental';

@Component({
  selector: 'app-my-rental',
  templateUrl: './my-rental.component.html',
  styleUrls: ['./my-rental.component.scss'],
})
export class MyRentalComponent {
  rentalList: RentalDto[] = [];
  searchTerm: string = '';
  currentUser: string = '';

  constructor(
    private rentalService: RentalService,
    private configStateService: ConfigStateService
  ) {}

  ngOnInit() {
    this.getListRental();
  }

  // searchRentals() {
  //   if (this.searchTerm) {
  //     const input = {
  //       maxResultCount: 20,
  //       sorting: 'rentalDate desc',
  //       filter: { userName: this.currentUser }, // Ekle
  //     };

  //     this.rentalService
  //       .getFilterByNameByUserNameAndInput(this.searchTerm, input)
  //       .subscribe(response => {
  //         this.rentalList = response.items;
  //       });
  //   } else {
  //     this.getListRental();
  //   }
  // }

  onSearchTermChange() {
    if (!this.searchTerm) {
      this.getListRental();
    }
  }

  formatRentalDate(date: string): string {
    return formatDate(date, 'dd MMM yyyy', 'tr');
  }

  getListRental() {
    this.configStateService.getOne$('currentUser').subscribe(user => {
      const input = {
        maxResultCount: 50,
        sorting: 'rentalDate desc',
      };

      this.rentalService
        .getFilterByNameByUserNameAndInput(user.userName, input)
        .subscribe(response => {
          this.rentalList = response.items;
        });
    });
  }
}
