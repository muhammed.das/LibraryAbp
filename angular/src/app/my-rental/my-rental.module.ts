import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyRentalRoutingModule } from './my-rental-routing.module';
import { MyRentalComponent } from './my-rental.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [MyRentalComponent],
  imports: [SharedModule, MyRentalRoutingModule],
})
export class MyRentalModule {}
