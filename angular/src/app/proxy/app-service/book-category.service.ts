import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { BookCategoryDto, CreateUpdateBookCategoryDto } from '../dtos/book-category/models';

@Injectable({
  providedIn: 'root',
})
export class BookCategoryService {
  apiName = 'Default';
  

  create = (input: CreateUpdateBookCategoryDto) =>
    this.restService.request<any, BookCategoryDto>({
      method: 'POST',
      url: '/api/app/book-category',
      body: input,
    },
    { apiName: this.apiName });
  

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/book-category/${id}`,
    },
    { apiName: this.apiName });
  

  get = (id: string) =>
    this.restService.request<any, BookCategoryDto>({
      method: 'GET',
      url: `/api/app/book-category/${id}`,
    },
    { apiName: this.apiName });
  

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<BookCategoryDto>>({
      method: 'GET',
      url: '/api/app/book-category',
      params: { sorting: input.sorting, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName });
  

  update = (id: string, input: CreateUpdateBookCategoryDto) =>
    this.restService.request<any, BookCategoryDto>({
      method: 'PUT',
      url: `/api/app/book-category/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
