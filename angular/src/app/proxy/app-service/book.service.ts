import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { BookDto, CreateUpdateBookDto } from '../dtos/book/models';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  apiName = 'Default';

  create = (createBook: CreateUpdateBookDto) =>
    this.restService.request<any, BookDto>(
      {
        method: 'POST',
        url: '/api/app/book',
        body: createBook,
      },
      { apiName: this.apiName }
    );

  delete = (id: string) =>
    this.restService.request<any, void>(
      {
        method: 'DELETE',
        url: `/api/app/book/${id}`,
      },
      { apiName: this.apiName }
    );

  get = (id: string) =>
    this.restService.request<any, BookDto>(
      {
        method: 'GET',
        url: `/api/app/book/${id}`,
      },
      { apiName: this.apiName }
    );

  getFilterByNameByBookNameAndInput = (bookName: string, input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<BookDto>>(
      {
        method: 'GET',
        url: '/api/app/book/filter-by-name',
        params: {
          bookName,
          sorting: input.sorting,
          skipCount: input.skipCount,
          maxResultCount: input.maxResultCount,
        },
      },
      { apiName: this.apiName }
    );

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<BookDto>>(
      {
        method: 'GET',
        url: '/api/app/book',
        params: {
          sorting: input.sorting,
          skipCount: input.skipCount,
          maxResultCount: input.maxResultCount,
        },
      },
      { apiName: this.apiName }
    );

  update = (id: string, updateBook: CreateUpdateBookDto) =>
    this.restService.request<any, BookDto>(
      {
        method: 'PUT',
        url: `/api/app/book/${id}`,
        body: updateBook,
      },
      { apiName: this.apiName }
    );

  constructor(private restService: RestService) {}
}
