import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { CreateUpdateRentalUserDto, RentalUserDto } from '../dtos/rental-user/models';

@Injectable({
  providedIn: 'root',
})
export class RentalUserService {
  apiName = 'Default';
  

  create = (input: CreateUpdateRentalUserDto) =>
    this.restService.request<any, RentalUserDto>({
      method: 'POST',
      url: '/api/app/rental-user',
      body: input,
    },
    { apiName: this.apiName });
  

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/rental-user/${id}`,
    },
    { apiName: this.apiName });
  

  get = (id: string) =>
    this.restService.request<any, RentalUserDto>({
      method: 'GET',
      url: `/api/app/rental-user/${id}`,
    },
    { apiName: this.apiName });
  

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<RentalUserDto>>({
      method: 'GET',
      url: '/api/app/rental-user',
      params: { sorting: input.sorting, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName });
  

  update = (id: string, input: CreateUpdateRentalUserDto) =>
    this.restService.request<any, RentalUserDto>({
      method: 'PUT',
      url: `/api/app/rental-user/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
