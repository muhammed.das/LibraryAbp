import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { CreateUpdateRentalDto, RentalDto } from '../dtos/rental/models';

@Injectable({
  providedIn: 'root',
})
export class RentalService {
  apiName = 'Default';
  

  create = (createRental: CreateUpdateRentalDto) =>
    this.restService.request<any, RentalDto>({
      method: 'POST',
      url: '/api/app/rental',
      body: createRental,
    },
    { apiName: this.apiName });
  

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/rental/${id}`,
    },
    { apiName: this.apiName });
  

  get = (id: string) =>
    this.restService.request<any, RentalDto>({
      method: 'GET',
      url: `/api/app/rental/${id}`,
    },
    { apiName: this.apiName });
  

  getFilterByNameByUserNameAndInput = (userName: string, input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<RentalDto>>({
      method: 'GET',
      url: '/api/app/rental/filter-by-name',
      params: { userName, sorting: input.sorting, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName });
  

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<RentalDto>>({
      method: 'GET',
      url: '/api/app/rental',
      params: { sorting: input.sorting, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName });
  

  update = (id: string, updateRental: CreateUpdateRentalDto) =>
    this.restService.request<any, RentalDto>({
      method: 'PUT',
      url: `/api/app/rental/${id}`,
      body: updateRental,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
