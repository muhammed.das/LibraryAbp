import type { EntityDto } from '@abp/ng.core';

export interface AuthorDto extends EntityDto<string> {
  name?: string;
}

export interface CreateUpdateAuthorDto {
  name?: string;
}
