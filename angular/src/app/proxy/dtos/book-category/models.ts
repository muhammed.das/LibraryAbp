import type { EntityDto } from '@abp/ng.core';

export interface BookCategoryDto extends EntityDto<string> {
  categoryId?: string;
  bookId?: string;
}

export interface CreateUpdateBookCategoryDto {
  categoryId?: string;
  bookId?: string;
}
