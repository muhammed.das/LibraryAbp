import type { EntityDto } from '@abp/ng.core';

export interface BookDto extends EntityDto<string> {
  name?: string;
  stock: number;
  numberPage: number;
  authorName?: string;
  shelFloor?: string;
  shelfNumber?: string;
  categoryName: string[];
}

export interface CreateUpdateBookDto {
  name?: string;
  stock: number;
  numberPage: number;
  authorName?: string;
  shelFloor?: string;
  shelfNumber?: string;
  categoryName: string[];
}
