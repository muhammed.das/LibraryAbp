import * as Author from './author';
import * as Book from './book';
import * as BookCategory from './book-category';
import * as Rental from './rental';
import * as RentalUser from './rental-user';
import * as Shelf from './shelf';
export * from './models';
export { Author, Book, BookCategory, Rental, RentalUser, Shelf };
