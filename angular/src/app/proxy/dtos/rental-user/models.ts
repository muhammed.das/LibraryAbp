import type { EntityDto } from '@abp/ng.core';

export interface CreateUpdateRentalUserDto {
  rentalId?: string;
  userId?: string;
}

export interface RentalUserDto extends EntityDto<string> {
  rentalId?: string;
  userId?: string;
}
