import type { EntityDto } from '@abp/ng.core';

export interface CreateUpdateRentalDto {
  bookName?: string;
  rentalDate?: string;
  returnDate?: string;
  status: boolean;
  userName?: string;
}

export interface RentalDto extends EntityDto<string> {
  bookName?: string;
  rentalDate?: string;
  returnDate?: string;
  status: boolean;
  userName?: string;
  rentalCount?: number;
}
