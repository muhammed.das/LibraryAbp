import type { EntityDto } from '@abp/ng.core';

export interface CreateUpdateShelfDto {
  floor?: string;
  number?: string;
}

export interface ShelfDto extends EntityDto<string> {
  floor?: string;
  number?: string;
}
