import {
  ConfigStateService,
  ListService,
  PagedAndSortedResultRequestDto,
  PagedResultDto,
  PermissionService,
} from '@abp/ng.core';
import {
  GetIdentityUsersInput,
  IdentityUserDto,
  IdentityUserService,
} from '@abp/ng.identity/proxy';
import { Confirmation, ConfirmationService, ToasterService } from '@abp/ng.theme.shared';
import { formatDate } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RentalService } from '@proxy/app-service';
import { CreateUpdateRentalDto, RentalDto } from '@proxy/dtos/rental';

@Component({
  selector: 'app-rental',
  templateUrl: './rental.component.html',
  styleUrls: ['./rental.component.scss'],
  providers: [ListService],
})
export class RentalComponent {
  rental = { items: [], totalCount: 0 } as PagedResultDto<RentalDto>;

  selectedRental = {} as RentalDto;

  searchTerm: string = '';

  filteredUsers: IdentityUserDto[] = [];
  selectedUser: IdentityUserDto;

  isModalOpen = false;

  formGroup: FormGroup;
  user: IdentityUserDto[] = [];

  constructor(
    public readonly list: ListService,
    private rentalService: RentalService,
    private formBuilder: FormBuilder,
    private confirmation: ConfirmationService,
    private configStateService: ConfigStateService,
    private permissionService: PermissionService,
    private toastrService: ToasterService,
    private userService: IdentityUserService
  ) {}
  ngOnInit() {
    this.getListRental();
    this.getUsers();
  }

  getUsers() {
    this.userService.getList({ maxResultCount: 1 }).subscribe(response => {
      const totalCount = response.totalCount;
      this.userService.getList({ maxResultCount: totalCount }).subscribe(response => {
        this.user = response.items;
        this.filteredUsers = response.items; // filteredUsers dizisini doldur
      });
    });
  }
  SearchTermChange(event: any) {
    const searchTerm = event;
    console.log(searchTerm); // searchTerm değerini kontrol etmek için
    if (!searchTerm) {
      this.filteredUsers = this.user;
    } else if (typeof searchTerm === 'string') {
      // Event değeri bir dize mi diye kontrol et
      const lowerCaseSearchTerm = searchTerm.toLowerCase();
      this.filteredUsers = this.user.filter(user =>
        user.userName.toLowerCase().includes(lowerCaseSearchTerm)
      );
    }
  }

  hasEditPermission: boolean = this.permissionService.getGrantedPolicy('ExampAbp.Library.Edit');
  hasDeletePermission: boolean = this.permissionService.getGrantedPolicy('ExampAbp.Library.Delete');

  searchRentals() {
    const input: PagedAndSortedResultRequestDto = {
      maxResultCount: 25,
    };

    if (this.searchTerm) {
      this.rentalService
        .getFilterByNameByUserNameAndInput(this.searchTerm, input)
        .subscribe(response => {
          this.rental = response;
        });
    } else {
      this.getListRental();
    }
  }

  onSearchTermChange() {
    if (!this.searchTerm) {
      this.getListRental();
    }
  }

  getStatus(status: boolean): string {
    return status ? 'Teslim Edildi' : 'Teslim Edilmedi';
  }

  formatRentalDate(date: string): string {
    return formatDate(date, 'dd MMM yyyy', 'tr');
  }

  getListRental() {
    const rentalStreamCreator = query => this.rentalService.getList(query);

    this.list.hookToQuery(rentalStreamCreator).subscribe(response => {
      this.rental = response;
    });
  }

  createRental() {
    this.selectedRental = {} as RentalDto;
    this.buildForm();
    this.isModalOpen = true;
  }

  editRental(rental: RentalDto) {
    this.selectedRental = { ...rental };
    this.buildForm();
    this.isModalOpen = true;
  }
  updateStatus(rental: RentalDto) {
    rental.status = true; // Statusu "Teslim Edildi" olarak güncelle

    this.rentalService.update(rental.id, rental).subscribe(() => {
      this.toastrService.success('Kitap Teslim Edildi');
      // Gerekirse yeniden listeleyin veya sayfayı güncelleyin
    });
  }

  buildForm() {
    const userNameControl = this.formBuilder.control(
      { value: '', disabled: false },
      Validators.required
    );

    if (this.selectedRental.id && this.selectedUser) {
      userNameControl.setValue(this.selectedUser.userName);
    }

    this.formGroup = this.formBuilder.group({
      bookName: [this.selectedRental.bookName || '', Validators.required],
      rentalDate: [this.selectedRental.rentalDate || '', Validators.required],
      returnDate: [this.selectedRental.returnDate || '', Validators.required],
      status: [this.selectedRental.status || '', Validators.required],
      userName: userNameControl,
    });
  }
  save() {
    if (this.formGroup.invalid) {
      return;
    }

    const createUpdateDto: CreateUpdateRentalDto = {
      bookName: this.formGroup.value.bookName,
      rentalDate: this.formGroup.value.rentalDate,
      returnDate: this.formGroup.value.returnDate,
      status: this.formGroup.value.status,
      userName: this.selectedUser.userName, // Seçili kullanıcının userName özelliğini kullanarak userName'i kaydedin
    };

    if (this.selectedRental.id) {
      this.rentalService.update(this.selectedRental.id, createUpdateDto).subscribe(() => {
        this.isModalOpen = false;
        this.formGroup.reset();
        this.list.get();
        this.toastrService.success('Kiralama İşlemi başarıyla Güncellendi');
      });
    } else {
      this.rentalService.create(createUpdateDto).subscribe(() => {
        this.isModalOpen = false;
        this.formGroup.reset();
        this.list.get();
        this.toastrService.success('Kiralama İşlemi başarıyla Eklendi');
      });
    }
  }
  delete(id: string) {
    this.confirmation.warn('::AreYouSureToDelete', '::AreYouSure').subscribe(status => {
      if (status === Confirmation.Status.confirm) {
        this.rentalService.delete(id).subscribe(() => this.list.get());
        this.toastrService.success('Kiralama İşlemi başarıyla Silindi');
      }
    });
  }
}
