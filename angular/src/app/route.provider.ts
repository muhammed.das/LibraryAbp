import { RoutesService, eLayoutType } from '@abp/ng.core';
import { APP_INITIALIZER } from '@angular/core';

export const APP_ROUTE_PROVIDER = [
  { provide: APP_INITIALIZER, useFactory: configureRoutes, deps: [RoutesService], multi: true },
];

function configureRoutes(routesService: RoutesService) {
  return () => {
    routesService.add([
      {
        path: '/',
        name: '::Menu:Home',
        iconClass: 'fas fa-home',
        order: 1,
        layout: eLayoutType.application,
      },
      {
        path: '/category',
        name: 'Library',
        iconClass: 'fas fa-book',
        order: 2,
        layout: eLayoutType.application,
      },
      {
        path: '/categories',
        name: 'Categories',
        parentName: 'Library',
        layout: eLayoutType.application,
        requiredPolicy: 'ExampAbp.Library',
      },
      {
        path: '/authors',
        name: 'Authors',
        parentName: 'Library',
        layout: eLayoutType.application,
        requiredPolicy: 'ExampAbp.Library',
      },
      {
        path: '/shelves',
        name: 'Shelves',
        parentName: 'Library',
        layout: eLayoutType.application,
        requiredPolicy: 'ExampAbp.Library',
      },
      {
        path: '/books',
        name: 'Books',
        parentName: 'Library',
        layout: eLayoutType.application,
        requiredPolicy: 'ExampAbp.Library',
      },
      {
        path: '/rentals',
        name: 'Rentals',
        parentName: 'Library',
        layout: eLayoutType.application,
        requiredPolicy: 'ExampAbp.Library',
      },
      {
        path: '/BookList',
        name: 'Rental Store',
        iconClass: 'fas fa-shopping-bag',
        order: 3,
        layout: eLayoutType.application,
      },
      {
        path: '/myRentals',
        name: 'MyRentals',
        parentName: 'Rental Store',
        layout: eLayoutType.application,
      },
      {
        path: '/BookList',
        name: 'Book Rentals',
        parentName: 'Rental Store',
        layout: eLayoutType.application,
      },
    ]);
  };
}
