import { ListService, PagedResultDto, PermissionService } from '@abp/ng.core';
import { Confirmation, ConfirmationService, ToasterService } from '@abp/ng.theme.shared';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ShelfService } from '@proxy/app-service';
import { ShelfDto } from '@proxy/dtos/shelf';

@Component({
  selector: 'app-shelf',
  templateUrl: './shelf.component.html',
  styleUrls: ['./shelf.component.scss'],
  providers: [ListService],
})
export class ShelfComponent {
  shelf = { items: [], totalCount: 0 } as PagedResultDto<ShelfDto>;

  selectedShelf = {} as ShelfDto;

  isModalOpen = false;

  formGroup: FormGroup;

  constructor(
    public readonly list: ListService,
    private shelfService: ShelfService,
    private formBuilder: FormBuilder,
    private confirmation: ConfirmationService,
    private permissionService: PermissionService,
    private toastrService: ToasterService
  ) {}
  ngOnInit() {
    this.getListShelf();
  }

  hasEditPermission: boolean = this.permissionService.getGrantedPolicy('ExampAbp.Library.Edit');
  hasDeletePermission: boolean = this.permissionService.getGrantedPolicy('ExampAbp.Library.Delete');

  getListShelf() {
    const shelfStreamCreator = query => this.shelfService.getList(query);

    this.list.hookToQuery(shelfStreamCreator).subscribe(response => {
      this.shelf = response;
    });
  }

  createShelf() {
    this.selectedShelf = {} as ShelfDto;
    this.buildForm();
    this.isModalOpen = true;
  }

  editShelf(id: string) {
    this.shelfService.get(id).subscribe(shelf => {
      this.selectedShelf = shelf;
      this.buildForm();
      this.isModalOpen = true;
    });
  }

  buildForm() {
    this.formGroup = this.formBuilder.group({
      floor: [this.selectedShelf.floor || '', Validators.required],
      number: [this.selectedShelf.number || '', Validators.required],
    });
  }
  save() {
    if (this.formGroup.invalid) {
      return;
    }

    if (this.selectedShelf.id) {
      this.shelfService.update(this.selectedShelf.id, this.formGroup.value).subscribe(() => {
        this.isModalOpen = false;
        this.formGroup.reset();
        this.list.get();
        this.toastrService.success('Raf başarıyla Güncellendi.');
      });
    } else {
      this.shelfService.create(this.formGroup.value).subscribe(() => {
        this.isModalOpen = false;
        this.formGroup.reset();
        this.list.get();
        this.toastrService.success('Raf başarıyla Eklendi.');
      });
    }
  }

  delete(id: string) {
    this.confirmation.warn('::AreYouSureToDelete', '::AreYouSure').subscribe(status => {
      if (status === Confirmation.Status.confirm) {
        this.shelfService.delete(id).subscribe(() => this.list.get());
        this.toastrService.success('Raf başarıyla Silindi.');
      }
    });
  }
}
