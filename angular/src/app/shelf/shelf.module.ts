import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShelfRoutingModule } from './shelf-routing.module';
import { ShelfComponent } from './shelf.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ShelfComponent],
  imports: [SharedModule, ShelfRoutingModule],
})
export class ShelfModule {}
