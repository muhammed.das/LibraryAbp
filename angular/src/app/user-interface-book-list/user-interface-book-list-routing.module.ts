import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserInterfaceBookListComponent } from './user-interface-book-list.component';

const routes: Routes = [{ path: '', component: UserInterfaceBookListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserInterfaceBookListRoutingModule { }
