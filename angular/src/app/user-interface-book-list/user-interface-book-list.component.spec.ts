import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserInterfaceBookListComponent } from './user-interface-book-list.component';

describe('UserInterfaceBookListComponent', () => {
  let component: UserInterfaceBookListComponent;
  let fixture: ComponentFixture<UserInterfaceBookListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserInterfaceBookListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserInterfaceBookListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
