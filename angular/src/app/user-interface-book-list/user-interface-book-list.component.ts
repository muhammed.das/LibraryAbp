import {
  ConfigStateService,
  ListService,
  PagedAndSortedResultRequestDto,
  PagedResultDto,
} from '@abp/ng.core';
import { Confirmation, ConfirmationService, ToasterService } from '@abp/ng.theme.shared';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BookService, CategoryService, RentalService } from '@proxy/app-service';
import { CategoryDto } from '@proxy/dtos';
import { BookDto } from '@proxy/dtos/book';
import { RentalDto } from '@proxy/dtos/rental';

@Component({
  selector: 'app-user-interface-book-list',
  templateUrl: './user-interface-book-list.component.html',
  styleUrls: ['./user-interface-book-list.component.scss'],
  providers: [ListService],
})
export class UserInterfaceBookListComponent {
  categoryList: CategoryDto[];
  filteredBookList: PagedResultDto<BookDto> = { items: [], totalCount: 0 };

  searchTerm: string = '';

  isModalOpen = false;
  formGroup: FormGroup;
  selectedRental: RentalDto;

  constructor(
    private categoryService: CategoryService,
    private bookService: BookService,
    public readonly list: ListService,
    private rentalService: RentalService,
    private confirmation: ConfirmationService,
    private formBuilder: FormBuilder,
    private configStateService: ConfigStateService,
    private toastrService: ToasterService
  ) {}

  ngOnInit() {
    this.getCategoryList();
    this.getBookList();
  }

  searchBooks() {
    const input: PagedAndSortedResultRequestDto = {
      maxResultCount: 50,
    };

    if (this.searchTerm) {
      this.bookService
        .getFilterByNameByBookNameAndInput(this.searchTerm, input)
        .subscribe(response => {
          this.filteredBookList = response;
        });
    } else {
      this.getBookList();
    }
  }

  onSearchTermChange() {
    if (!this.searchTerm) {
      this.getBookList();
    }
  }

  showAllBooks() {
    this.getBookList();
  }

  getCategoryList() {
    this.categoryService
      .getList({ sorting: '', skipCount: 0, maxResultCount: 50 })
      .subscribe(response => {
        this.categoryList = response.items;
      });
  }
  getBookList() {
    const bookStreamCreator = query => this.bookService.getList(query);

    this.list.hookToQuery(bookStreamCreator).subscribe(response => {
      this.filteredBookList = response;
    });
  }

  getBooksByCategory(categoryName: string) {
    const input: PagedAndSortedResultRequestDto = {
      sorting: '',
      skipCount: 0,
      maxResultCount: 1000,
    };

    this.bookService.getFilterByNameByBookNameAndInput(categoryName, input).subscribe(response => {
      this.filteredBookList = response;
    });
  }
  createRental(book: BookDto) {
    this.selectedRental = {} as RentalDto;
    this.selectedRental.bookName = book.name; // Kitabın ismini RentalDto'ya ata
    this.buildForm();
    this.isModalOpen = true;
  }

  buildForm() {
    this.formGroup = this.formBuilder.group({
      bookName: [this.selectedRental.bookName || '', Validators.required],
      rentalDate: [this.selectedRental.rentalDate || '', Validators.required],
      returnDate: [this.selectedRental.returnDate || '', Validators.required],
      status: [{ value: false, disabled: true }],
      userName: [{ value: '', readonly: true }, Validators.required],
    });

    if (this.selectedRental.id) {
      this.formGroup.get('userName').setValue(this.selectedRental.userName);
    } else {
      this.configStateService.getOne$('currentUser').subscribe(user => {
        this.formGroup.get('userName').setValue(user.userName);
      });
    }
  }

  save() {
    if (this.formGroup.invalid) {
      return;
    }

    const createUpdateDto = this.formGroup.value;
    createUpdateDto.status = false; // "Teslim Edilmedi" olarak ayarlandı

    this.rentalService.create(createUpdateDto).subscribe(() => {
      this.isModalOpen = false;
      this.formGroup.reset();
      this.list.get();
      this.toastrService.success('Kiralama İşlemi başarıyla Gerçekleşti');
      this.toastrService.info('Lütfen Kitabınızı İlgili Raftan Alınız');
    });
  }
}
