import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserInterfaceBookListRoutingModule } from './user-interface-book-list-routing.module';
import { UserInterfaceBookListComponent } from './user-interface-book-list.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [UserInterfaceBookListComponent],
  imports: [SharedModule, UserInterfaceBookListRoutingModule],
})
export class UserInterfaceBookListModule {}
