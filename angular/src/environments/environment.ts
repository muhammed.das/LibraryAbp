import { Environment } from '@abp/ng.core';

const baseUrl = 'http://localhost:4200';

export const environment = {
  production: false,
  application: {
    baseUrl,
    name: 'ExampAbp',
    logoUrl: '',
  },
  oAuthConfig: {
    issuer: 'https://localhost:44314/',
    redirectUri: baseUrl,
    clientId: 'ExampAbp_App',
    responseType: 'code',
    scope: 'offline_access ExampAbp',
    requireHttps: true,
  },
  apis: {
    default: {
      url: 'https://localhost:44314',
      rootNamespace: 'ExampAbp',
    },
  },
} as Environment;
