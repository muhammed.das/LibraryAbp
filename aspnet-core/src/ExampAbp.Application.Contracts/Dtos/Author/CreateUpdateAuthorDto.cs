﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExampAbp.Dtos.Author
{
    public class CreateUpdateAuthorDto
    {
        public string? Name { get; set; }
    }
}
