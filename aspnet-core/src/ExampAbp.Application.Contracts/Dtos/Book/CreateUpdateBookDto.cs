﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExampAbp.Dtos.Book
{
    public class CreateUpdateBookDto
    {
        public string Name { get; set; }
        public int Stock { get; set; }
        public int NumberPage { get; set; }
        public string AuthorName { get; set; }
        public string ShelFloor { get; set; }="";
        public string ShelfNumber { get; set; }

        public List<string> CategoryName { get; set; }
    }
}
