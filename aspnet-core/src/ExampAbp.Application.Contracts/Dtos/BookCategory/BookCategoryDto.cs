﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace ExampAbp.Dtos.BookCategory
{
    public class BookCategoryDto:EntityDto<Guid>
    {
        public Guid CategoryId { get; set; }

        public Guid BookId { get; set; }
    }
}
