﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExampAbp.Dtos.BookCategory
{
    public class CreateUpdateBookCategoryDto
    {
        public Guid CategoryId { get; set; }

        public Guid BookId { get; set; }
    }
}
