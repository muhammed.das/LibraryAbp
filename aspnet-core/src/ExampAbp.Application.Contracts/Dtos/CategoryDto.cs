﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace ExampAbp.Dtos
{
    public class CategoryDto:EntityDto<Guid>
    {
        public string Name { get; set; }
    }
}
