﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExampAbp.Dtos
{
    public class CreateUpdateCategoryDto
    {
        public string Name { get; set; }
    }
}
