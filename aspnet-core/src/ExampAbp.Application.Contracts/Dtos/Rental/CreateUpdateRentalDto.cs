﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExampAbp.Dtos.Rental
{
    public class CreateUpdateRentalDto
    {
        public string BookName { get; set; }
        public DateTime RentalDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public bool Status { get; set; }

        public string UserName { get; set; }
    }
}
