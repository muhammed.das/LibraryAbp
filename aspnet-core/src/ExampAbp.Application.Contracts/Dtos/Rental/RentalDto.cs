﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace ExampAbp.Dtos.Rental
{
    public class RentalDto:EntityDto<Guid>
    {
        public string BookName { get; set; }
        public DateTime RentalDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public bool Status { get; set; }

        public string UserName { get; set; }
    }
}
