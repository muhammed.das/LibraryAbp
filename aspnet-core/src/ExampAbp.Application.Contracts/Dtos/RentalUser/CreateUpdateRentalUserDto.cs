﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExampAbp.Dtos.RentalUser
{
    public class CreateUpdateRentalUserDto
    {
        public Guid RentalId { get; set; }
        public Guid UserId { get; set; }
    }
}
