﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace ExampAbp.Dtos.RentalUser
{
    public class RentalUserDto:EntityDto<Guid>
    {
        public Guid RentalId { get; set; }
        public Guid UserId { get; set; }
    }
}
