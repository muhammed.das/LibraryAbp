﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExampAbp.Dtos.Shelf
{
    public class CreateUpdateShelfDto
    {
        public string? Floor { get; set; }
        public string? Number { get; set; }
    }
}
