﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace ExampAbp.Dtos.Shelf
{
    public class ShelfDto:EntityDto<Guid>
    {
        public string? Floor { get; set; }
        public string? Number { get; set; }
    }
}
