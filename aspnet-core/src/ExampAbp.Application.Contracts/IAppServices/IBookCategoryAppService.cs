﻿using ExampAbp.Dtos.BookCategory;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace ExampAbp.IAppServices
{
    public interface IBookCategoryAppService:ICrudAppService<BookCategoryDto,Guid,PagedAndSortedResultRequestDto,CreateUpdateBookCategoryDto>
    {
    }
}
