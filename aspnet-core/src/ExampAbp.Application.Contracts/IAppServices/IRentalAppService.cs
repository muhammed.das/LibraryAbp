﻿using ExampAbp.Dtos.Book;
using ExampAbp.Dtos.Rental;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace ExampAbp.IAppServices
{
    public interface IRentalAppService:ICrudAppService<RentalDto,Guid,PagedAndSortedResultRequestDto,CreateUpdateRentalDto>
    {
        PagedResultDto<RentalDto> GetFilterByName(string userName, PagedAndSortedResultRequestDto input);
    }
}
