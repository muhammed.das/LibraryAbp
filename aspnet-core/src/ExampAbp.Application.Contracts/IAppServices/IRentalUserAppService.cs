﻿using ExampAbp.Dtos.RentalUser;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace ExampAbp.IAppServices
{
    public interface IRentalUserAppService:ICrudAppService<RentalUserDto,Guid,PagedAndSortedResultRequestDto,CreateUpdateRentalUserDto>
    {
    }
}
