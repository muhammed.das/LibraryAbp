﻿using ExampAbp.Dtos.Shelf;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace ExampAbp.IAppServices
{
    public interface IShelfAppService:ICrudAppService<ShelfDto,Guid,PagedAndSortedResultRequestDto,CreateUpdateShelfDto>
    {
    }
}
