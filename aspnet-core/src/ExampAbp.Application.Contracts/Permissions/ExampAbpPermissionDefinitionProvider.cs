﻿using ExampAbp.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace ExampAbp.Permissions;

public class ExampAbpPermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var LibraryGroup = context.AddGroup(ExampAbpPermissions.GroupName, L("Permission:ExampAbp"));

        var libraryPermission = LibraryGroup.AddPermission(ExampAbpPermissions.Library.Default, L("Permission:Library"));
        libraryPermission.AddChild(ExampAbpPermissions.Library.Create, L("Permission:Library.Create"));
        libraryPermission.AddChild(ExampAbpPermissions.Library.Edit, L("Permission:Library.Edit"));
        libraryPermission.AddChild(ExampAbpPermissions.Library.Delete, L("Permission:Library.Delete"));
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<ExampAbpResource>(name);
    }
}
