﻿namespace ExampAbp.Permissions;

public static class ExampAbpPermissions
{
    public const string GroupName = "ExampAbp";

    public static class Library
    {
        public const string Default = GroupName + ".Library";
        public const string Create = Default + ".Create";
        public const string Edit = Default + ".Edit";
        public const string Delete = Default + ".Delete";
    }
}
