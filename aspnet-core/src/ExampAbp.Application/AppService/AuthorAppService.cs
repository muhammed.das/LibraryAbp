﻿using ExampAbp.Dtos.Author;
using ExampAbp.Entities;
using ExampAbp.IAppServices;
using ExampAbp.Permissions;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace ExampAbp.AppService
{
    public class AuthorAppService:CrudAppService<Author,AuthorDto,Guid,PagedAndSortedResultRequestDto,CreateUpdateAuthorDto>,IAuthorAppService
    {
        public AuthorAppService(IRepository<Author,Guid> repository):base(repository) 
        {
            GetPolicyName = ExampAbpPermissions.Library.Default;
            GetListPolicyName = ExampAbpPermissions.Library.Default;
            CreatePolicyName = ExampAbpPermissions.Library.Create;
            UpdatePolicyName = ExampAbpPermissions.Library.Edit;
            DeletePolicyName = ExampAbpPermissions.Library.Delete;
        }
    }
}
