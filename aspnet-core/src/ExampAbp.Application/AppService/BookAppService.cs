﻿using ExampAbp.Dtos.Book;
using ExampAbp.Entities;
using ExampAbp.IAppServices;
using ExampAbp.Permissions;
using ExampAbp.Repositories;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Linq;
using Volo.Abp.ObjectMapping;
using static System.Reflection.Metadata.BlobBuilder;

namespace ExampAbp.AppService
{
    [Authorize(ExampAbpPermissions.Library.Default)]
    public class BookAppService:CrudAppService<Book,BookDto,Guid,PagedAndSortedResultRequestDto,CreateUpdateBookDto>,IBookAppService
    {
        private readonly IBookRepository _bookRepository;
        private readonly IRepository<Author, Guid> _authorRepository;
        private readonly IRepository<Shelf, Guid> _shelfRepository;
        private readonly IRepository<Category, Guid> _categoryRepository;
        private readonly IRepository<BookCategory, Guid> _bookCategoryRepository;
        public BookAppService(
            IRepository<Book, Guid> repository,
            IBookRepository bookRepository,
            IRepository<BookCategory, Guid> bookCategoryRepository,
            IRepository<Author, Guid> authorRepository,
            IRepository<Category,Guid> categoryRepository,
            IRepository<Shelf, Guid> shelfRepository)
            : base(repository)
        {
            _bookCategoryRepository= bookCategoryRepository;
            _categoryRepository= categoryRepository;
            _bookRepository = bookRepository;
            _authorRepository = authorRepository;
            _shelfRepository = shelfRepository;
        }


        public override async Task<PagedResultDto<BookDto>> GetListAsync(PagedAndSortedResultRequestDto input)
        {
            var books = await _bookRepository.GetAllWithAuthorAsync();

            var totalCount = books.Count;
            var filteredBooks = books.Skip(input.SkipCount).Take(input.MaxResultCount);

            var result = filteredBooks.ToList().Select(book =>
            {
                var dto = ObjectMapper.Map<Book, BookDto>(book);
                dto.AuthorName = book.Author.Name;
                dto.ShelFloor = book.Shelf.Floor;
                dto.ShelfNumber = book.Shelf.Number;

                dto.CategoryName = book.Categories.Select(bc => bc.Category.Name).ToList();

                return dto;
            }).ToList();

            return new PagedResultDto<BookDto>(totalCount, result);
        }
        public PagedResultDto<BookDto> GetFilterByName(string bookName, PagedAndSortedResultRequestDto input)
        {
            var query = _bookRepository.GetAllByFilter(x => x.Name.Contains(bookName) || x.Author.Name.Contains(bookName) || x.Categories.Select(a=>a.Category.Name).Contains(bookName));

            var totalCount = query.Count();
            var filteredBooks = query.OrderBy(book => book.Name) 
                                    .Skip(input.SkipCount)
                                    .Take(input.MaxResultCount);

            var result = filteredBooks.ToList();

            var dtos = result.Select(book =>
            {
                var dto = ObjectMapper.Map<Book, BookDto>(book);
                dto.AuthorName = book.Author.Name;
                dto.ShelFloor = book.Shelf.Floor;
                dto.ShelfNumber = book.Shelf.Number;
                dto.CategoryName = book.Categories.Select(bc => bc.Category.Name).ToList();
                return dto;
            }).ToList();

            return new PagedResultDto<BookDto>(totalCount, dtos);
        }

        public override async Task<BookDto> CreateAsync(CreateUpdateBookDto createBook)
        {
            var existingBook = await _bookRepository.FirstOrDefaultAsync(x=>x.Name == createBook.Name);

            if (existingBook != null)
            {
                throw new UserFriendlyException("Bu İsimde Kitap Zaten Mevcut");
            }   

            var author = await _authorRepository.FirstOrDefaultAsync(x => x.Name == createBook.AuthorName);
            if (author == null)
            {
                throw new UserFriendlyException("Kullanıcı Bulunamadı.");
            }

            var shelf = await _shelfRepository.FirstOrDefaultAsync(x => x.Number == createBook.ShelfNumber && x.Floor == createBook.ShelFloor);
            if (shelf == null)
            {
                throw new UserFriendlyException("İlgili Raf Bulunamadı.");
            }

            var book = ObjectMapper.Map<CreateUpdateBookDto, Book>(createBook);
            book.Author = author;
            book.Shelf = shelf;

            await _bookRepository.InsertAsync(book);

            foreach (var categoryName in createBook.CategoryName)
            {
                var category = await _categoryRepository.FirstOrDefaultAsync(x => x.Name == categoryName);

                if (category != null)
                {
                    var bookCategory = new BookCategory
                    {
                        BookId = book.Id,
                        CategoryId = category.Id,
                    };

                    await _bookCategoryRepository.InsertAsync(bookCategory);
                }
            }

            var createdBookDto = MapToGetOutputDto(book);
            createdBookDto.AuthorName = createBook.AuthorName;
            createdBookDto.ShelFloor = createBook.ShelFloor;
            createdBookDto.ShelfNumber = createBook.ShelfNumber;
            createdBookDto.CategoryName = createBook.CategoryName;

            return createdBookDto;
        }


        public override async Task<BookDto> UpdateAsync(Guid id, CreateUpdateBookDto updateBook)
        {
            var book = await _bookRepository.GetAsync(id);
            if (book == null)
            {
                throw new UserFriendlyException("Kitap Güncellenemedi.");
            }

            var author = await _authorRepository.FirstOrDefaultAsync(x => x.Name == updateBook.AuthorName);
            if (author == null)
            {
                throw new UserFriendlyException("Yazar Güncellenemedi.");
            }

            var shelf = await _shelfRepository.FirstOrDefaultAsync(x => x.Number == updateBook.ShelfNumber && x.Floor == updateBook.ShelFloor);
            if (shelf == null)
            {
                throw new UserFriendlyException("İlgili Raf Güncellenemedi.");
            }

            ObjectMapper.Map(updateBook, book);
            book.AuthorId = author.Id;
            book.Shelf = shelf;

            await Repository.UpdateAsync(book);
            var bookCategories = await _bookCategoryRepository.GetListAsync(bc => bc.BookId == book.Id);
            foreach (var bookCategory in bookCategories)
            {
                await _bookCategoryRepository.DeleteAsync(bookCategory);
            }

            foreach (var categoryName in updateBook.CategoryName)
            {
                var category = await _categoryRepository.FirstOrDefaultAsync(x => x.Name == categoryName);
                if (category != null)
                {
                    var bookCategory = new BookCategory
                    {
                        BookId = book.Id,
                        CategoryId = category.Id
                    };

                    await _bookCategoryRepository.InsertAsync(bookCategory);
                }
            }

            var updatedBookDto = MapToGetOutputDto(book);
            updatedBookDto.AuthorName = author.Name;
            updatedBookDto.ShelFloor = shelf.Floor;
            updatedBookDto.ShelfNumber = shelf.Number;
            updatedBookDto.CategoryName = updateBook.CategoryName;

            return updatedBookDto;
        }

    
    }


}





