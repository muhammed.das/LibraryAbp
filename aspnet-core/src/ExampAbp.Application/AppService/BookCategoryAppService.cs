﻿using ExampAbp.Dtos.BookCategory;
using ExampAbp.Entities;
using ExampAbp.IAppServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace ExampAbp.AppService
{
    public class BookCategoryAppService:CrudAppService<BookCategory,BookCategoryDto,Guid, PagedAndSortedResultRequestDto,CreateUpdateBookCategoryDto>,IBookCategoryAppService
    {
        public BookCategoryAppService(IRepository<BookCategory,Guid> repository):base(repository)
        {
        
    
        }




    }
}
