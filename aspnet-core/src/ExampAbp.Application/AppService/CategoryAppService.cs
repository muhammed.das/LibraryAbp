﻿using ExampAbp.Dtos;
using ExampAbp.Entities;
using ExampAbp.IAppServices;
using ExampAbp.Permissions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace ExampAbp.AppService
{
    public class CategoryAppService : CrudAppService<Category, CategoryDto, Guid, PagedAndSortedResultRequestDto, CreateUpdateCategoryDto>, ICategoryAppService
    {
        public CategoryAppService(IRepository<Category, Guid> repository) : base(repository)
        {
            GetPolicyName = ExampAbpPermissions.Library.Default;
            GetListPolicyName = ExampAbpPermissions.Library.Default;
            CreatePolicyName = ExampAbpPermissions.Library.Create;
            UpdatePolicyName = ExampAbpPermissions.Library.Edit;
            DeletePolicyName = ExampAbpPermissions.Library.Delete;
        }
    }
}
