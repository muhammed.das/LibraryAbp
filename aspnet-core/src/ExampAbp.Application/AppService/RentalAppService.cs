﻿using ExampAbp.Dtos.Book;
using ExampAbp.Dtos.Rental;
using ExampAbp.Entities;
using ExampAbp.IAppServices;
using ExampAbp.Permissions;
using ExampAbp.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Identity;

namespace ExampAbp.AppService
{
    public class RentalAppService:CrudAppService<Rental,RentalDto,Guid,PagedAndSortedResultRequestDto,CreateUpdateRentalDto>, IRentalAppService
    {
        private readonly IRentalRepository _rentalRepository;
        private readonly IBookRepository _bookRepository;
        private readonly UserManager<IdentityUser> _userManager;
       private readonly IRepository<RentalUser, Guid> _rentalUserRepository;
        public RentalAppService(IRepository<Rental,Guid> repository,IRentalRepository rentalRepository,
            IBookRepository bookRepository, UserManager<IdentityUser> userManager,
            IRepository<RentalUser,Guid> rentalUserRepository) :base(repository) 
        {
            _rentalUserRepository= rentalUserRepository;
            _bookRepository = bookRepository;
            _userManager= userManager;
            _rentalRepository= rentalRepository;
        }

        public override async Task<PagedResultDto<RentalDto>> GetListAsync(PagedAndSortedResultRequestDto input)
        {
            var rentals = await _rentalRepository.GetAllWithBookAndUserAsync();

            var totalCount = rentals.Count;
            var filteredRentals = rentals.Skip(input.SkipCount).Take(input.MaxResultCount);

            var result = filteredRentals.ToList().Select(rental =>
            {
                var dto = ObjectMapper.Map<Rental, RentalDto>(rental);
                dto.BookName = rental.Book.Name;
                dto.UserName = rental.Users.Select(u=>u.User.UserName).FirstOrDefault();

                return dto;
            }).ToList();

            return new PagedResultDto<RentalDto>(totalCount, result);
        }

        public override async Task<RentalDto> CreateAsync(CreateUpdateRentalDto createRental)
        {
            var book = await _bookRepository.FirstOrDefaultAsync(x => x.Name == createRental.BookName);
            if (book == null)
            {
                throw new UserFriendlyException("Kitap Bulunamadı.");
            }


            var rentalDuration = createRental.ReturnDate - createRental.RentalDate;

            if (createRental.ReturnDate <= createRental.RentalDate || rentalDuration.TotalDays>15)
            {
                throw new UserFriendlyException("Kiralama Zamanı En Fazla 15 Gün Aralığında Olabilir.Lütfen Tarihi Düzeltin");
            }

            if (book.Stock <= 0)
            {
                throw new UserFriendlyException("Kitap Stokta Yok.");
            }
            else
            {
                book.Stock--;
            }
            await _bookRepository.UpdateAsync(book);


            var user = await _userManager.FindByNameAsync(createRental.UserName);
            if (user == null)
            {
                throw new UserFriendlyException("İlgili Kullanıcı Bulunamadı.");
            }

            var rental = ObjectMapper.Map<CreateUpdateRentalDto, Rental>(createRental);
            rental.Book = book;
            rental.Users = new List<RentalUser> { new RentalUser { UserId = user.Id } };

            await _rentalRepository.InsertAsync(rental);


            var createdRentalDto = MapToGetOutputDto(rental);
            createdRentalDto.BookName = createRental.BookName;
            createdRentalDto.UserName = createRental.UserName;

            return createdRentalDto;
        }

        [Authorize(ExampAbpPermissions.Library.Edit)]
        public override async Task<RentalDto> UpdateAsync(Guid id, CreateUpdateRentalDto updateRental)
        {
            var rental = await Repository.GetAsync(id);
            if (rental == null)
            {
                throw new UserFriendlyException("Kiralama Güncellenemedi.");
            }

            var book = await _bookRepository.FirstOrDefaultAsync(x => x.Name == updateRental.BookName);
            if (book == null)
            {
                throw new UserFriendlyException("Kitap Güncellenemedi.");
            }

            var user = await _userManager.FindByNameAsync(updateRental.UserName);
            if (user == null)
            {
                throw new UserFriendlyException("İlgili Kullanıcı Güncellenemedi.");
            }

            var isStatusChanged = rental.Status != updateRental.Status;
            var previousStatus = rental.Status;

            ObjectMapper.Map(updateRental, rental);
            rental.BookId = book.Id;

            await Repository.UpdateAsync(rental);

            if (isStatusChanged && updateRental.Status && !previousStatus)
            {
                book.Stock++;
                await _bookRepository.UpdateAsync(book);
            }
            else if (isStatusChanged && !updateRental.Status && previousStatus)
            {
                book.Stock--;
                await _bookRepository.UpdateAsync(book);
            }

            var rentalUsers = await _rentalUserRepository.GetAsync(bc => bc.RentalId == rental.Id);
            await _rentalUserRepository.DeleteAsync(rentalUsers);

            var rentalUser = new RentalUser
            {
                RentalId = rental.Id,
                UserId = user.Id
            };

            await _rentalUserRepository.InsertAsync(rentalUser);

            var updatedRentalDto = MapToGetOutputDto(rental);
            updatedRentalDto.BookName = book.Name;
            updatedRentalDto.UserName = user.UserName;

            return updatedRentalDto;
        }

        [Authorize(ExampAbpPermissions.Library.Delete)]

        public override async Task DeleteAsync(Guid id)
        {
            var rent = await _rentalRepository.GetAsync(id);
            if (rent == null)
            {
                throw new UserFriendlyException("Kiralama Silinemedi.");
            }

            var book = await _bookRepository.GetAsync(rent.BookId);

            if (rent.Status == false)
            {
                book.Stock++;
                await _bookRepository.UpdateAsync(book);
            }

            await base.DeleteAsync(id);
        }

        public PagedResultDto<RentalDto> GetFilterByName(string userName, PagedAndSortedResultRequestDto input)
        {
            var query = _rentalRepository.GetAllByFilter(x => x.Book.Name.Contains(userName) || x.Users.Any(a => a.User.UserName.Contains(userName)));

            var totalCount = query.Count();
            var filteredRentals = query.OrderBy(rental => rental.Id)
                                       .Skip(input.SkipCount)
                                       .Take(input.MaxResultCount);

            var result = filteredRentals.ToList();

            var dtos = result.Select(rental =>
            {
                var dto = ObjectMapper.Map<Rental, RentalDto>(rental);
                dto.BookName = rental.Book.Name;
                dto.UserName = rental.Users.Select(u => u.User.UserName).FirstOrDefault();
                return dto;
            }).ToList();

            return new PagedResultDto<RentalDto>(totalCount, dtos);
        }
    }
}
