﻿using ExampAbp.Dtos.RentalUser;
using ExampAbp.Entities;
using ExampAbp.IAppServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace ExampAbp.AppService
{
    public class RentalUserAppService:CrudAppService<RentalUser,RentalUserDto,Guid,PagedAndSortedResultRequestDto,CreateUpdateRentalUserDto>,IRentalUserAppService
    {
        public RentalUserAppService(IRepository<RentalUser,Guid> repository):base(repository) 
        {

        }
    }
}
