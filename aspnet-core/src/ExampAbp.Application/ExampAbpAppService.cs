﻿using System;
using System.Collections.Generic;
using System.Text;
using ExampAbp.Localization;
using Volo.Abp.Application.Services;

namespace ExampAbp;

/* Inherit your application services from this class.
 */
public abstract class ExampAbpAppService : ApplicationService
{
    protected ExampAbpAppService()
    {
        LocalizationResource = typeof(ExampAbpResource);
    }
}
