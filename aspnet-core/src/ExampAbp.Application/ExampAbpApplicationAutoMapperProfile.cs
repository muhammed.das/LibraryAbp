﻿using AutoMapper;
using ExampAbp.Dtos;
using ExampAbp.Dtos.Author;
using ExampAbp.Dtos.Book;
using ExampAbp.Dtos.BookCategory;
using ExampAbp.Dtos.Rental;
using ExampAbp.Dtos.RentalUser;
using ExampAbp.Dtos.Shelf;
using ExampAbp.Entities;

namespace ExampAbp;

public class ExampAbpApplicationAutoMapperProfile : Profile
{
    public ExampAbpApplicationAutoMapperProfile()
    {
        CreateMap<Category, CategoryDto>().ReverseMap();
        CreateMap<Category, CreateUpdateCategoryDto>().ReverseMap();

        CreateMap<Author, AuthorDto>().ReverseMap();
        CreateMap<Author, CreateUpdateAuthorDto>().ReverseMap();

        CreateMap<Shelf, ShelfDto>().ReverseMap();
        CreateMap<Shelf,CreateUpdateShelfDto>().ReverseMap();

        CreateMap<Book, BookDto>().ForMember(a=>a.AuthorName,o=>o.MapFrom(ab=>ab.Author.Name)).ReverseMap();
        CreateMap<Book, CreateUpdateBookDto>().ForMember(a => a.AuthorName, o => o.MapFrom(ab => ab.Author.Name)).ReverseMap();

        CreateMap<BookCategory, BookCategoryDto>().ReverseMap();
        CreateMap<BookCategory, CreateUpdateBookCategoryDto>().ReverseMap();

        CreateMap<Rental, RentalDto>().ReverseMap();
        CreateMap<Rental, CreateUpdateRentalDto>().ReverseMap();

        CreateMap<RentalUser, RentalUserDto>().ReverseMap();
        CreateMap<RentalUser, CreateUpdateRentalUserDto>().ReverseMap();

    }
}
