﻿using ExampAbp.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace ExampAbp.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(ExampAbpEntityFrameworkCoreModule),
    typeof(ExampAbpApplicationContractsModule)
    )]
public class ExampAbpDbMigratorModule : AbpModule
{

}
