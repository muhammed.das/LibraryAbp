﻿using Volo.Abp.Localization;

namespace ExampAbp.Localization;

[LocalizationResourceName("ExampAbp")]
public class ExampAbpResource
{

}
