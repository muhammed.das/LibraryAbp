﻿using System.Threading.Tasks;

namespace ExampAbp.Data;

public interface IExampAbpDbSchemaMigrator
{
    Task MigrateAsync();
}
