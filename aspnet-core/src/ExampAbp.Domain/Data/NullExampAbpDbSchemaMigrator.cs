﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace ExampAbp.Data;

/* This is used if database provider does't define
 * IExampAbpDbSchemaMigrator implementation.
 */
public class NullExampAbpDbSchemaMigrator : IExampAbpDbSchemaMigrator, ITransientDependency
{
    public Task MigrateAsync()
    {
        return Task.CompletedTask;
    }
}
