﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;

namespace ExampAbp.Entities
{
    public class Book:Entity<Guid>
    {
        public string Name { get; set; }
        public int Stock { get; set; }
        public int NumberPage { get; set; }
        public Guid AuthorId { get; set; }
        public Guid ShelfId { get; set; }
        public Author Author { get; set; }
        public Shelf Shelf { get; set; }

        public List<BookCategory> Categories { get; set; }

    }
}
