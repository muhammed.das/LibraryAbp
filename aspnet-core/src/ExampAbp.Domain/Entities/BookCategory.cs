﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;

namespace ExampAbp.Entities
{
    public class BookCategory:Entity<Guid>
    {
        public Guid CategoryId { get; set; }

        public Guid BookId { get; set; }

        public Category Category { get; set; }
        public Book Book { get; set; }
    }
}
