﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;

namespace ExampAbp.Entities
{
    public class Category:Entity<Guid>
    {
        public string? Name { get; set; }

        public List<BookCategory> Books { get; set; }
    }
}
