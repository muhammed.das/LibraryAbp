﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;

namespace ExampAbp.Entities
{
    public class Rental:Entity<Guid>
    {
        public Guid BookId { get; set; }
        public DateTime RentalDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public bool Status { get; set; }

        public Book Book { get; set; }
        public List<RentalUser> Users { get; set; }
    }
}
