﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Identity;

namespace ExampAbp.Entities
{
    public class RentalUser:Entity<Guid>
    {
        public Guid RentalId { get; set; }

        public Guid UserId { get; set; }

        public Rental Rental { get; set; }
        public IdentityUser User { get; set; }
    }
}
