﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;

namespace ExampAbp.Entities
{
    public class Shelf:Entity<Guid>
    {
        public string? Floor { get; set; }
        public string? Number { get; set; }

        public List<Book> Books { get; set; }
    }
}
