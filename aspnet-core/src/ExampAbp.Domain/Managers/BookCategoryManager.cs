﻿using ExampAbp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace ExampAbp.Managers
{
    public class BookCategoryManager:DomainService
    {
        private readonly IBookCategoryRepository _bookCategory;

        public BookCategoryManager(IBookCategoryRepository bookCategory)
        {
            _bookCategory = bookCategory;
        }
    }
}
