﻿using ExampAbp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace ExampAbp.Managers
{
    public class RentalUserManager:DomainService
    {
        private readonly IRentalUserRepository _rentalUserRepository;

        public RentalUserManager(IRentalUserRepository rentalUserRepository)
        {
            _rentalUserRepository = rentalUserRepository;
        }
    }
}
