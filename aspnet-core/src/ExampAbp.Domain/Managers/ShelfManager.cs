﻿using ExampAbp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace ExampAbp.Managers
{
    public class ShelfManager:DomainService
    {
        private readonly IShelfRepository _shelfRepository;

        public ShelfManager(IShelfRepository shelfRepository)
        {
            _shelfRepository = shelfRepository;
        }
    }
}
