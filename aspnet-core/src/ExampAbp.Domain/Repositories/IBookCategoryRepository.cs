﻿using ExampAbp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace ExampAbp.Repositories
{
    public interface IBookCategoryRepository:IRepository<BookCategory,Guid>
    {

    }
}
