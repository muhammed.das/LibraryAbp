﻿using ExampAbp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace ExampAbp.Repositories
{
    public interface IBookRepository:IRepository<Book,Guid>
    {
        Task<List<Book>> GetAllWithAuthorAsync();
        IQueryable<Book> GetAllByFilter(Expression<Func<Book, bool>> filter = null);


    }
}
