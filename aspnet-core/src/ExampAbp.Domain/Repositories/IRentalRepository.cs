﻿using ExampAbp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace ExampAbp.Repositories
{
    public interface IRentalRepository:IRepository<Rental,Guid>
    {
        Task<List<Rental>> GetAllWithBookAndUserAsync();

        IQueryable<Rental> GetAllByFilter(Expression<Func<Rental, bool>> filter = null);

    }
}
