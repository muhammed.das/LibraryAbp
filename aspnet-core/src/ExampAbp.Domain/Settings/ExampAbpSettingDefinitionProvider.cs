﻿using Volo.Abp.Settings;

namespace ExampAbp.Settings;

public class ExampAbpSettingDefinitionProvider : SettingDefinitionProvider
{
    public override void Define(ISettingDefinitionContext context)
    {
        //Define your own settings here. Example:
        //context.Add(new SettingDefinition(ExampAbpSettings.MySetting1));
    }
}
