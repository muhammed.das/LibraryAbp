﻿using ExampAbp.Entities;
using ExampAbp.EntityFrameworkCore;
using ExampAbp.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace ExampAbp.EfRepository
{
    public class BookRepository : EfCoreRepository<ExampAbpDbContext, Book, Guid>, IBookRepository
    {
        ExampAbpDbContext _dbContext;
        public BookRepository(IDbContextProvider<ExampAbpDbContext> dbContextProvider, ExampAbpDbContext dbContext)
       : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }

        public IQueryable<Book> GetAllByFilter(Expression<Func<Book, bool>> filter = null)
        {
            var query = _dbContext.Books.Include(x => x.Categories)
                                .ThenInclude(bc => bc.Category).Include(b => b.Author).Include(s => s.Shelf).AsQueryable();
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query;
        }

        public async Task<List<Book>> GetAllWithAuthorAsync()
        {
            var books = await _dbContext.Set<Book>()
           .Include(b => b.Author)
           .Include(b => b.Shelf)
           .Include(b => b.Categories)
           .ThenInclude(bc => bc.Category)
           .ToListAsync();

            return books;
        }
    }
}
