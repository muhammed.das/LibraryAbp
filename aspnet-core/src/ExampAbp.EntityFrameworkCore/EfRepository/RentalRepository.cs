﻿using ExampAbp.Entities;
using ExampAbp.EntityFrameworkCore;
using ExampAbp.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace ExampAbp.EfRepository
{
    public class RentalRepository : EfCoreRepository<ExampAbpDbContext,Rental, Guid>, IRentalRepository
    {
        ExampAbpDbContext _dbContext;
        public RentalRepository(IDbContextProvider<ExampAbpDbContext> dbContextProvider, ExampAbpDbContext dbContext)
       : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }

        public IQueryable<Rental> GetAllByFilter(Expression<Func<Rental, bool>> filter = null)
        {
            var query = _dbContext.Rentals.Include(x => x.Users)
                              .ThenInclude(bc => bc.User).Include(b => b.Book).AsQueryable();
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query;
        }

        public async Task<List<Rental>> GetAllWithBookAndUserAsync()
        {
            var rentals = await _dbContext.Set<Rental>()
           .Include(b => b.Book)
           .Include(b => b.Users)
           .ThenInclude(a=>a.User)
           .ToListAsync();
          

            return rentals;
        }
    }
}
