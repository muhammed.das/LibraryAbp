﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace ExampAbp;

[Dependency(ReplaceServices = true)]
public class ExampAbpBrandingProvider : DefaultBrandingProvider
{
    public override string AppName => "ExampAbp";
}
