﻿using ExampAbp.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace ExampAbp.Controllers;

/* Inherit your controllers from this class.
 */
public abstract class ExampAbpController : AbpControllerBase
{
    protected ExampAbpController()
    {
        LocalizationResource = typeof(ExampAbpResource);
    }
}
