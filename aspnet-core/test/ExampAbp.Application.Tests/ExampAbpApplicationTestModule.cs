﻿using Volo.Abp.Modularity;

namespace ExampAbp;

[DependsOn(
    typeof(ExampAbpApplicationModule),
    typeof(ExampAbpDomainTestModule)
    )]
public class ExampAbpApplicationTestModule : AbpModule
{

}
