﻿using ExampAbp.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace ExampAbp;

[DependsOn(
    typeof(ExampAbpEntityFrameworkCoreTestModule)
    )]
public class ExampAbpDomainTestModule : AbpModule
{

}
